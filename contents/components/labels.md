---
name: Labels
related:
  - tags
---

Labels allow users to categorize UI objects using descriptive titles and customizable color. They provide a quick way to recognize which category or categories the labeled object belongs. Clicking on a label navigates the user to a filtered list view based on that label.

## Usage

Labels are used to categorize issues, merge requests, and epics. They can be used to filter in list views. Upon hover, a tooltip containing a description of the label will appear.

### User role labels

User role labels are different from standard labels. They indicate the permission level of the user and differ visually. User labels lack color, use grayscale text, and have a border.

## Demo

Todo: Add live component block with code example

## Design specifications

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for labels](https://gitlab-org.gitlab.io/gitlab-design/hosted/design-gitlab-specs/labels-spec-previews/)
